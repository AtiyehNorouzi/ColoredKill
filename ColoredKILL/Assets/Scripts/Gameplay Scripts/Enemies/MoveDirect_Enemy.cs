﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDirect_Enemy : IMovable
{
  
    protected override IEnumerator Move(Vector3 dir, float Speed)
    {
        while (_moving)
        {    
            transform.Translate(dir * Speed * TimeManager.deltaTime);
            yield return null;
        }
    }

    public override void Dispawn()
    {
        _moving = false;
        GenericDirectEnemyPooler.instance.pushEnemy(this);
    }

 
}
