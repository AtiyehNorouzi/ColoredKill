﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericStopEnemyPooler : MonoBehaviour
{


    #region poolObjectsStruct
    [System.Serializable]
    public struct stopEnemyObjStruct
    {
        public int amount;
        public GameObject prefab;
        public Stack<StopAndGo_Enemy> pooledObjs;
    }
    #endregion
    public stopEnemyObjStruct enemyWeWantToPool;
    GameObject enemyParent;

    // Use this for initialization
    private static GenericStopEnemyPooler _instance;
    public static GenericStopEnemyPooler instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GenericStopEnemyPooler>();

            return _instance;
        }
    }
    void Awake()
    {
        _instance = this;
        CreatePools();
    }
    public void CreatePools()
    {
        createBullets();
    }
    public void createBullets()
    {
        enemyWeWantToPool.pooledObjs = new Stack<StopAndGo_Enemy>();
        enemyParent = new GameObject(enemyWeWantToPool.prefab.name + "_parent");
        enemyParent.transform.SetParent(instance.gameObject.transform);

        for (int j = 0; j < enemyWeWantToPool.amount; j++)
        {

            GameObject GO = Instantiate(enemyWeWantToPool.prefab);
            StopAndGo_Enemy c = GO.GetComponent<StopAndGo_Enemy>();
            GO.SetActive(false);
            GO.transform.SetParent(enemyParent.transform);
            enemyWeWantToPool.pooledObjs.Push(c);
        }

    }


    public void pushEnemy(StopAndGo_Enemy enemy)
    {
        enemy.gameObject.SetActive(false);
        enemyWeWantToPool.pooledObjs.Push(enemy);
        enemy.gameObject.transform.SetParent(enemyParent.transform);
        enemy.gameObject.transform.localPosition = Vector3.zero;
    }


    public StopAndGo_Enemy getEnemy()
    {
        StopAndGo_Enemy p = enemyWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;
    }


}
