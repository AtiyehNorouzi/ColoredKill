﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateChilds_Enemy : Strong_Enemy
{

    public override void Dispawn()
    {
        _healthbar.fillAmount = 1;
        GenericBirthEnemyPooler.instance.pushEnemy(this);
    }

    protected override void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "bullet")
        {
            IMovable _movable = col.gameObject.GetComponent<IMovable>();
            if (_movable.GetColor().Equals(_color))
            {
                _lifes--;
                _healthbar.fillAmount -= _lifeDecrease;
                if (_healthbar.fillAmount <= 0.5f)
                {
                    //birth three childs:)
                    for (int i = -1; i < 2; i++)
                    {
                        MoveDirect_Enemy _moveDirect = GenericDirectEnemyPooler.instance.getEnemy();
                        _moveDirect.transform.position = transform.position + new Vector3(i * 5, 0, i * 10);
                        _moveDirect.transform.rotation = transform.rotation;
                        _moveDirect.Init((_characterPos.position - transform.position).normalized, 10f, getRandomColor());
                    }
                    Dispawn();
                }
                else if (Utility.almostEqual(_healthbar.fillAmount, 0, 0.01f))
                {
                    Dispawn();
                    _moving = false;
                }
            }

        }
        if (col.gameObject.tag == "character")
            Dispawn();
    }
    Color getRandomColor()
    {
        int rand = Random.Range(0, 2);
        if (rand == 0)
            return Color.red;
        return Color.blue;
    }
}
