﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Strong_Enemy : MoveDirect_Enemy  
{
    [SerializeField]
    protected Transform _characterPos;
    [SerializeField]
    protected Image _healthbar;
    protected float _lifeDecrease;
    protected float _lifes;
    protected Vector3 _dir;
    protected bool _onSwitch;
    public virtual void Init(Vector3 dir, float Speed, Color color, float dead_bulletNumbers)
    {
        _lifeDecrease = 1f / dead_bulletNumbers;
        _lifes = dead_bulletNumbers;
        _dir = dir;
        base.Init(dir, Speed, color);
    }
    protected override IEnumerator Move(Vector3 dir, float Speed)
    {
        while (_moving)
        {
            if (!_onSwitch)          
                transform.Translate(_dir * Speed * TimeManager.deltaTime);
            yield return null;
        }
    }
    public override void Dispawn()
    {
        _healthbar.fillAmount = 1;
        GenericStrongEnemyPooler.instance.pushEnemy(this);
    }

    public virtual IEnumerator Switch(Transform a, Transform b, float MoveTime)
    {
        Vector3 _aTemp = a.position;
        Vector3 _bTemp = b.position;
        float _timer = 0;
        while (_onSwitch)
        {
            if (!Utility.almostEqual(a.position, _bTemp, 2f))
            {
                _timer += TimeManager.deltaTime * 1 / MoveTime;
                a.position = Vector3.Lerp(_aTemp, _bTemp, _timer);
            }
            else
            {
                _dir = (_characterPos.position - a.position).normalized;
                _onSwitch = false;
            }
            yield return null;
        }

    }
    protected override void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "bullet")
        {
            IMovable _movable = col.gameObject.GetComponent<IMovable>();
            if (_movable.GetColor().Equals(_color))
            {
                _lifes--;
                _healthbar.fillAmount -= _lifeDecrease;
                if (Utility.almostEqual(_healthbar.fillAmount, 0, 0.01f))
                {
                    Dispawn();
                    _moving = false;
                }
            }     

        }
        if (col.gameObject.tag == "character")
            Dispawn();
    }
    public float getRemainderLifes()
    {
        return _lifes;
    }
    public void MustSwitch(bool canSwitch , Transform dist ,  float moveTime)
    {
         _onSwitch = canSwitch;
        if(gameObject != null && dist != null && gameObject.activeSelf == true)
            StartCoroutine(Switch(transform , dist , moveTime));
    }
}

