﻿using UnityEngine;
using System.Collections;

public class RotateObject : MonoBehaviour
{
    [SerializeField]
    private Transform _planeTransform;
    private Plane _plane;
    private const float _rotateSensivity = 10f;
    public static Vector3 pointToLook;
    void Update()
    {
    
        Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        float rayLength;
        _plane = new Plane(Vector3.up, _planeTransform.position);
        if(_plane.Raycast(cameraRay , out rayLength))
        {
            pointToLook = cameraRay.GetPoint(rayLength);
            Vector3 distance = pointToLook - transform.position;
            Quaternion newRotation = Quaternion.LookRotation(distance);
            transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, TimeManager.deltaTime * _rotateSensivity);

        }
    }
  

}
