﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Character : MonoBehaviour
{
    public delegate void Deleg_Shoot();
    public event Deleg_Shoot _onShoot;
    [SerializeField]
    private InputController _inputController;
    [SerializeField]
    private Transform _ShootLocation;
    [SerializeField]
    private Animation _animation;
    [SerializeField]
    private Image heart_slider;
    private const float _minusHeart = 0.35f;
    private string[] _animationNames = { "Wait", "Attack", "Dead", "Damage" };
    void Start ()
    {
        playCharacterAnimation(1, QueueMode.PlayNow);
        _inputController._OnClicked += InputListener;
	}
	void InputListener(InputController.InputType type , Color color)
    {
        if(type == InputController.InputType.Shoot )
            if(TimeToShootSlider.isFilled)
                Shoot(color);       
    }

    void Shoot(Color color)
    {
        if (_onShoot != null)
            _onShoot();
        playCharacterAnimation(1, QueueMode.CompleteOthers);
        Bullet bullet = GenericBulletPooler.instance.GetBullet();
        bullet.gameObject.transform.position = _ShootLocation.position;
        bullet.gameObject.transform.rotation = _ShootLocation.rotation;
        bullet.Init(RotateObject.pointToLook - transform.position, LevelController.GetCurrentBulletPower() , color);
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "enemy")
        {
            heart_slider.fillAmount -= _minusHeart;
            playCharacterAnimation(3, QueueMode.CompleteOthers);
            if (heart_slider.fillAmount <= 0)
            {
                playCharacterAnimation(2, QueueMode.PlayNow);
                TimeManager.timeScale = 0;
                gameStateController.instance.ActiveRetryPanel();
            }

        }
    }
    void OnLevelWasLoaded()
    {
        _onShoot = null;
    }
    void playCharacterAnimation(int index , QueueMode mode)
    {
        _animation.PlayQueued(_animationNames[index], mode);
    }
}
