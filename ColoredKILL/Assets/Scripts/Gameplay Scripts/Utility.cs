﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility : MonoBehaviour
{
    public static bool almostEqual(float a, float b, float dist)
    {
        if (Mathf.Abs(a - b) >= dist)
            return false;
        return true;
    }
    public static bool almostEqual(Vector3 a , Vector3 b , float dist)
    {
        if (!almostEqual(a.x , b.x , dist)) return false;
        else if (!almostEqual(a.y, b.y, dist)) return false;
        else if (!almostEqual(a.z, b.z, dist)) return false;
        return true;
    }
    public static float getDistance(float x1 , float z1 , float x2 , float z2)
    {
        return Mathf.Sqrt(Mathf.Pow(x1 - x2, 2) + Mathf.Pow(z1 - z2, 2));
    }

}
