﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopAndGo_Enemy : IMovable
{
    [SerializeField]
    private Transform _characterPos;
    private Vector3 _middlePos;
    private const float _stayTime = 2f;
    private bool _inMiddle;
    public override void Init(Vector3 dir, float Speed, Color color)
    {
        _middlePos = (_characterPos.position + transform.position) / 2f;
        base.Init(dir, Speed, color);     
    }

    protected override IEnumerator Move(Vector3 dir, float Speed)
    {
        while(_moving)
        {
            //musit change distance compare     
            if (Vector3.Distance(transform.position , _middlePos) <= 5f && !_inMiddle)
            {
                yield return new WaitForSeconds(_stayTime);
                _inMiddle = true;
            }
            transform.Translate(dir * Speed * TimeManager.deltaTime);
            yield return null;
        }
   
    }

    public override void Dispawn()
    {
        GenericStopEnemyPooler.instance.pushEnemy(this);
    }
}
