﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : IMovable
{
    //up , right, bottom , left
    [SerializeField]
    private Transform[] _zones;
    public override void Init(Vector3 dir, float Speed, Color color)
    {
        base.Init(dir, Speed, color);
        StartCoroutine(CheckBulletZone());
    }
    protected override void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "enemy")
        {
            _moving = false;
            Dispawn();
        }
    }
    protected override IEnumerator Move(Vector3 dir, float Speed)
    {
        while (_moving)
        {
            transform.Translate(dir * Speed * TimeManager.deltaTime, Space.World);
            yield return null;
        }
    }
    public override void Dispawn()
    {
        GenericBulletPooler
            .instance.pushBullet(this);
    }
    public IEnumerator CheckBulletZone()
    {
        while (_moving)
        {
            if (transform.position.z >= _zones[0].position.z|| transform.position.z <= _zones[2].position.z ||
                transform.position.x >= _zones[1].position.x || transform.position.x <= _zones[3].position.x)
            {
                _moving = false;
                Dispawn();
            }
            yield return null;
        }
    }
}
