﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger_Enemy : MoveDirect_Enemy
{
    
    private float _changeColorTime;
    public void Init(Vector3 dir, float Speed, Color color , float ChangeColorTime)
    {
        base.Init( dir,  Speed,  color);
        _changeColorTime = ChangeColorTime;
        StartCoroutine(ChangeColor());
    }

    public override void Dispawn()
    {
        GenericColorEnemyPooler.instance.pushEnemy(this);
    }
    IEnumerator ChangeColor()
    {
        while(true)
        {
            yield return new WaitForSeconds(_changeColorTime);
            _color = GetSwitchedColor();
            GetComponent<MeshRenderer>().material.color = _color;
            yield return null;
        }
    }
    Color GetSwitchedColor()
    {
        if (_color == Color.red)
            return Color.blue;
        return Color.red;
    }
}
